## PERFORMANCE-TEST


### Summary Response k6

| Item                     | Significado                                      | Possíveis Impactos                               |
|--------------------------|--------------------------------------------------|--------------------------------------------------|
| data_received            | Quantidade de dados recebidos pelo k6 em bytes. | Pode indicar a carga de dados do teste e afetar a performance da aplicação alvo. |
| data_sent                | Quantidade de dados enviados pelo k6 em bytes.  | Pode indicar a carga de dados enviados pela aplicação e afetar a performance do servidor. |
| http_req_blocked         | Tempo médio que uma solicitação HTTP passa bloqueada antes de ser enviada. | Alto tempo de bloqueio pode indicar problemas de rede ou de carga do servidor. |
| http_req_connecting      | Tempo médio para estabelecer uma conexão TCP com o servidor. | Alto tempo de conexão pode indicar problemas de rede ou sobrecarga do servidor. |
| http_req_duration        | Tempo médio total para completar uma solicitação HTTP. | Indica o tempo médio gasto para fazer uma solicitação HTTP, incluindo todas as etapas (conexão, envio de requisição, recebimento de resposta, etc.). |
| http_req_failed          | Porcentagem de solicitações HTTP que falharam.     | Um alto número de falhas pode indicar problemas na aplicação ou na infraestrutura. |
| http_req_receiving       | Tempo médio para receber a resposta HTTP.           | Indica o tempo médio gasto para receber a resposta HTTP após enviar a requisição. |
| http_req_sending         | Tempo médio para enviar a requisição HTTP.         | Indica o tempo médio gasto para enviar a requisição HTTP após a conexão ter sido estabelecida. |
| http_req_tls_handshaking | Tempo médio para concluir o handshake TLS.         | Indica o tempo médio gasto para estabelecer uma conexão segura (HTTPS) com o servidor. |
| http_req_waiting         | Tempo médio para aguardar a resposta do servidor após a requisição ter sido enviada. | Indica o tempo médio gasto para aguardar a resposta do servidor após enviar a requisição. |
| http_reqs                | Número total de solicitações HTTP feitas.          | Indica a carga total de solicitações HTTP durante o teste. |
| iteration_duration       | Tempo médio total de uma iteração do teste.       | Indica o tempo médio gasto para executar uma iteração completa do teste, incluindo todas as solicitações HTTP. |
| iterations               | Número total de iterações do teste.               | Indica o número total de vezes que o teste foi repetido. |
| vus                      | Número atual de usuários virtuais ativos.         | Indica o número atual de usuários virtuais simulados pelo k6. |
| vus_max                  | Número máximo de usuários virtuais configurados.  | Indica o número máximo de usuários virtuais que podem ser simulados pelo k6. |


---------

### Como agir em cima dos resultados dos testes de performance

1. **Análise dos dados recebidos e enviados:**
   - **Interpretação:** Verifique se a quantidade de dados enviados e recebidos está dentro das expectativas. Isso pode ajudar a identificar possíveis gargalos de rede ou problemas de carga na aplicação.
   - **Ação:** Se os volumes de dados forem muito altos, considere otimizar o tamanho das respostas do servidor ou a quantidade de dados enviados pelas requisições para melhorar a eficiência do teste.

2. **Tempo de bloqueio das requisições HTTP:**
   - **Interpretação:** O tempo de bloqueio indica quanto tempo as requisições ficaram bloqueadas antes de serem enviadas. Altos tempos de bloqueio podem indicar problemas de rede ou de sobrecarga do servidor.
   - **Ação:** Se os tempos de bloqueio forem elevados, investigue a infraestrutura de rede ou ajuste a configuração do servidor para lidar com mais requisições simultâneas.

3. **Tempo de conexão e handshake TLS:**
   - **Interpretação:** Essas métricas indicam o tempo necessário para estabelecer a conexão com o servidor e realizar o handshake TLS, respectivamente. Altos tempos podem indicar problemas de rede ou de desempenho do servidor.
   - **Ação:** Se os tempos de conexão ou de handshake forem altos, verifique a configuração do servidor e otimize a infraestrutura de rede, se necessário.

4. **Tempo de espera e duração das requisições HTTP:**
   - **Interpretação:** O tempo de espera é quanto tempo uma requisição espera pela resposta do servidor, enquanto a duração total inclui o tempo de espera mais o tempo de processamento do servidor. Altos tempos podem indicar problemas de desempenho da aplicação.
   - **Ação:** Se os tempos de espera ou duração forem altos, analise o código da aplicação e otimize consultas a bancos de dados, operações de E/S ou outras operações que possam estar causando atrasos.

5. **Porcentagem de falhas de requisições HTTP:**
   - **Interpretação:** Indica a porcentagem de solicitações HTTP que falharam durante o teste. Altas taxas de falha podem indicar problemas na aplicação ou na infraestrutura.
   - **Ação:** Se a taxa de falha for alta, identifique as requisições específicas que estão falhando e investigue a causa raiz, como erros de codificação, configuração incorreta do servidor ou recursos insuficientes.

6. **Número de usuários virtuais (VUs):**
   - **Interpretação:** Representa o número atual de usuários virtuais ativos simulados pelo k6. Altos números podem indicar uma carga pesada no servidor.
   - **Ação:** Se o número de VUs estiver causando problemas de desempenho, considere otimizar a aplicação, adicionar mais recursos de servidor ou ajustar a configuração de VUs para simular uma carga mais realista.

7. **Número de requisições HTTP e iterações:**
   - **Interpretação:** Indica a carga total de solicitações HTTP e o número total de iterações do teste, respectivamente.
   - **Ação:** Analise esses números para entender a carga de trabalho imposta à aplicação durante o teste. Se necessário, ajuste a configuração do teste para simular uma carga mais realista ou otimizar a aplicação para lidar com a carga atual.

---------

Claro, aqui está um guia sobre como criar critérios de aceitação para diferentes aplicações, levando em consideração seus cenários e períodos:

### Criando Critérios de Aceitação para Cada Aplicação

1. **Entendimento das Aplicações:**
   - **Importância:** Antes de definir critérios de aceitação, é crucial entender as peculiaridades de cada aplicação, incluindo sua finalidade, arquitetura, público-alvo e requisitos de desempenho.
   - **Ação:** Realize uma análise detalhada das aplicações, converse com as partes interessadas e identifique os principais cenários de uso e períodos de pico de atividade.

2. **Identificação de Cenários de Uso:**
   - **Importância:** Cada aplicação pode ter diferentes cenários de uso, que exigem diferentes níveis de desempenho e disponibilidade.
   - **Ação:** Identifique os cenários de uso mais comuns e críticos para cada aplicação. Isso pode incluir transações de alta frequência, acesso a recursos específicos, períodos de tráfego intenso, entre outros.

3. **Definição de Métricas de Desempenho:**
   - **Importância:** As métricas de desempenho são essenciais para avaliar se uma aplicação atende aos critérios de aceitação.
   - **Ação:** Determine as métricas de desempenho relevantes para cada cenário de uso, como tempo de resposta, taxa de transferência, latência, taxa de erro, entre outras.

4. **Estabelecimento de Critérios de Aceitação:**
   - **Importância:** Os critérios de aceitação ajudam a definir o nível mínimo de desempenho e disponibilidade que uma aplicação deve alcançar para ser considerada aceitável.
   - **Ação:** Com base nos cenários de uso e nas métricas de desempenho identificadas, estabeleça critérios de aceitação claros e mensuráveis para cada aplicação. Por exemplo, um critério de aceitação pode ser "o tempo de resposta médio para transações críticas não deve exceder 1 segundo durante os períodos de pico de tráfego".

5. **Consideração dos Períodos de Pico:**
   - **Importância:** Os períodos de pico de atividade podem ter requisitos de desempenho diferentes dos períodos de baixa demanda.
   - **Ação:** Leve em consideração os períodos de pico de atividade ao definir os critérios de aceitação. Por exemplo, você pode estabelecer critérios de desempenho mais rigorosos durante os períodos de pico para garantir uma experiência consistente para os usuários.

6. **Validação e Ajuste Contínuos:**
   - **Importância:** Os critérios de aceitação devem ser validados regularmente e ajustados conforme necessário para garantir que permaneçam relevantes e alinhados com as necessidades do negócio e dos usuários.
   - **Ação:** Realize testes de desempenho regulares para validar se as aplicações atendem aos critérios de aceitação estabelecidos. Se necessário, ajuste os critérios com base nos resultados dos testes ou em mudanças nas demandas do usuário ou na infraestrutura da aplicação.

----------------------

### Exemplos de Cenários

````js
import http from 'k6/http';

export const options = {
  hosts: { 'test.k6.io': '1.2.3.4' },
  stages: [
    { duration: '1m', target: 10 },
    { duration: '1m', target: 20 },
    { duration: '1m', target: 0 },
  ],
  thresholds: { http_req_duration: ['avg<100', 'p(95)<200'] },
  noConnectionReuse: true,
  userAgent: 'MyK6UserAgentString/1.0',
};

export default function () {
  http.get('http://test.k6.io/');
}

````


1. **Cenário de Carga Constante:**
   - **Descrição:** Este cenário simula uma carga constante na aplicação, mantendo um número constante de usuários virtuais ao longo do teste.
   - **Options:**
     - `stages`: Configure um estágio com duração indefinida e um alvo de usuários virtuais constante.
     - `thresholds`: Defina limites de desempenho para garantir que a aplicação atenda aos critérios de aceitação durante toda a execução do teste.

2. **Cenário de Escalonamento Gradual:**
   - **Descrição:** Este cenário aumenta gradualmente o número de usuários virtuais ao longo do tempo para avaliar como a aplicação lida com uma carga crescente.
   - **Options:**
     - `stages`: Configure vários estágios com diferentes alvos de usuários virtuais e durações para aumentar gradualmente a carga na aplicação.
     - `thresholds`: Defina limites de desempenho para cada estágio, garantindo que a aplicação mantenha um desempenho aceitável à medida que a carga aumenta.

3. **Cenário de Explosão de Tráfego:**
   - **Descrição:** Este cenário simula uma súbita explosão de tráfego na aplicação para testar sua capacidade de lidar com picos de atividade.
   - **Options:**
     - `stages`: Configure um estágio com uma duração curta e um alvo de usuários virtuais muito alto para criar uma carga intensa durante um curto período.
     - `thresholds`: Defina limites de desempenho para garantir que a aplicação responda rapidamente e não falhe sob uma carga pesada.

4. **Cenário de Ciclo de Pico e Vazamento:**
   - **Descrição:** Este cenário alterna entre períodos de pico de atividade e períodos de inatividade para testar a capacidade da aplicação de lidar com flutuações na demanda.
   - **Options:**
     - `stages`: Configure estágios alternados com diferentes níveis de tráfego para simular picos de atividade e períodos de baixa demanda.
     - `thresholds`: Defina limites de desempenho para cada fase do ciclo, garantindo que a aplicação se comporte conforme o esperado em diferentes cenários.

### Opções Adicionais

- `hosts`: Mapeia nomes de host para endereços IP, útil para testes em ambientes de desenvolvimento ou pré-produção.
- `thresholds`: Define limites de desempenho para métricas específicas, permitindo a verificação automática do desempenho da aplicação durante o teste.
- `noConnectionReuse`: Desativa a reutilização de conexões TCP entre solicitações HTTP para simular um comportamento mais realista do navegador.
- `userAgent`: Define a string de agente do usuário para todas as solicitações HTTP, permitindo a simulação de diferentes tipos de navegadores ou dispositivos.

------


### Exemplos de Configurações


1. **`vus`:**
   - Descrição: Especifica o número total de usuários virtuais (VUs) a serem simulados durante o teste.
   - Exemplo: `vus: 100`

2. **`duration`:**
   - Descrição: Define a duração total do teste, ou seja, quanto tempo o teste será executado.
   - Exemplo: `duration: '10m'` (para 10 minutos)

3. **`stages`:**
   - Descrição: Permite definir diferentes etapas de carga durante o teste, especificando o número de VUs e a duração de cada etapa.
   - Exemplo: 
     ```javascript
     stages: [
       { duration: '1m', target: 10 },
       { duration: '2m', target: 50 },
       { duration: '1m', target: 0 },
     ]
     ```

4. **`thresholds`:**
   - Descrição: Define critérios de aceitação para métricas específicas. O teste falhará se os valores das métricas ultrapassarem os limites definidos.
   - Exemplo: 
     ```javascript
     thresholds: {
       http_req_duration: ['avg<100', 'p(95)<200']
     }
     ```

5. **`discardResponseBodies`:**
   - Descrição: Se definido como `true`, os corpos de resposta HTTP não serão armazenados em memória, o que economiza recursos.
   - Exemplo: `discardResponseBodies: true`

6. **`tlsVersion`:**
   - Descrição: Define a versão do protocolo TLS a ser usada nas conexões HTTPS.
   - Exemplo: `tlsVersion: 'tls1.2'`

7. **`noConnectionReuse`:**
   - Descrição: Se definido como `true`, as conexões TCP não serão reutilizadas, o que pode ajudar a simular cenários de alta carga.
   - Exemplo: `noConnectionReuse: true`

8. **`maxRedirects`:**
   - Descrição: Define o número máximo de redirecionamentos HTTP permitidos durante as solicitações.
   - Exemplo: `maxRedirects: 5`

9. **`discardResponseBodies`:**
   - Descrição: Se definido como `true`, os corpos de resposta HTTP não serão armazenados em memória, o que economiza recursos.
   - Exemplo: `discardResponseBodies: true`

Esses são apenas alguns exemplos de parâmetros que podem ser configurados no objeto `options` do k6. A documentação do k6 fornece uma lista completa de opções disponíveis, juntamente com explicações detalhadas sobre como cada uma afeta o comportamento do teste.

> https://k6.io/docs/using-k6/k6-options/reference/



## K6 x Gatling

Tanto o k6 quanto o Gatling são ferramentas populares para realização de testes de carga e performance, mas têm algumas diferenças em suas abordagens e funcionalidades. Vou destacar algumas semelhanças e diferenças entre eles:

### Similaridades:

1. **Testes de Performance:**
   - Ambos k6 e Gatling são projetados para realizar testes de carga e performance em sistemas web.

2. **Scripting:**
   - Ambos os frameworks permitem escrever scripts para simular interações de usuários com a aplicação sob teste.

3. **Suporte a Protocolos:**
   - Ambos suportam protocolos como HTTP e HTTPS, permitindo testar aplicações web.

4. **Integração com Ferramentas de Monitoramento:**
   - Ambos os frameworks podem ser integrados com ferramentas de monitoramento, como Grafana, para visualização e análise dos resultados dos testes.

### Diferenças:

1. **Modelo de Concorrência:**
   - O k6 utiliza um modelo de execução baseado em Go-routines, permitindo uma execução simultânea mais eficiente de múltiplas solicitações.
   - Gatling utiliza um modelo de execução baseado em Akka actors, que também suporta execução concorrente, mas com uma abordagem diferente.

2. **Linguagem de Scripting:**
   - O k6 utiliza JavaScript como sua linguagem de script, facilitando para aqueles familiarizados com JavaScript.
   - Gatling utiliza uma DSL (Domain Specific Language) baseada em Scala, o que pode ser mais familiar para desenvolvedores com experiência em Scala.

3. **Facilidade de Uso:**
   - k6 é frequentemente considerado mais simples de configurar e usar devido à sua sintaxe JavaScript e à sua abordagem mais orientada a desenvolvedores.
   - Gatling pode ter uma curva de aprendizado um pouco mais íngreme, especialmente para aqueles menos familiarizados com Scala.

## Escolha

Ambos k6 e Gatling podem ser integrados com sistemas de monitoramentos para visualização e análise dos resultados dos testes de performance. No entanto, devido à sua sintaxe JavaScript e à facilidade de uso geral, o k6 foi escolhido pode ser mais amigável para a integração com Grafana, especialmente para aqueles que já estão familiarizados com JavaScript.
