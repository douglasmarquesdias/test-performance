import http from 'k6/http';
import { sleep } from 'k6';

export const options = {
  vus: 5,
  duration: '15s',
  ext: {
    loadimpact: {
      // Code Project
      projectID: 3684461,
      // Test runs with the same name groups test runs together.
      name: 'api-service'
    }
  }
};

export default function() {
  http.get('http://test.k6.io');
  sleep(1);
}