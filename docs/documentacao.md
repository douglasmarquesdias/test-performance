### PERFORMANCE-TEST

- [Tecnologias Utilizadas](#tecnologias-utilizadas)
- [A Importância de Fazer Testes de Performance](#a-importância-de-fazer-testes-de-performance)
- [Configuração de Pipeline](#configuração-de-pipeline)
    - [GITLAB-CI](#gitlab-ci)
    - [TEMPLATE](#template)
- [Conclusão](#conclusão)

## Tecnologias Utilizadas

1. k6
2. grafana
3. gitlab-ci

## A Importância de Fazer Testes de Performance

A Importância de Fazer Testes de Performance
Nos ambientes digitais cada vez mais complexos e competitivos de hoje, garantir que um aplicativo, site ou sistema funcione de forma eficiente e confiável é crucial para o sucesso do negócio. Os testes de desempenho desempenham um papel fundamental nesse processo, permitindo que as equipes de desenvolvimento e operações identifiquem e solucionem problemas relacionados ao desempenho antes que eles afetem os usuários finais.

1. Identificação de Gargalos
Os testes de desempenho ajudam a identificar gargalos e pontos de estrangulamento no sistema, como tempos de resposta lentos, alta utilização de recursos do servidor, consultas de banco de dados demoradas ou falhas de escalabilidade. Identificar esses problemas precocemente permite que as equipes de desenvolvimento atuem proativamente para corrigi-los antes que impactem negativamente a experiência do usuário.

2. Melhoria da Experiência do Usuário
Uma experiência do usuário lenta ou instável pode levar à frustração dos usuários, redução da satisfação do cliente e até mesmo perda de receita. Ao realizar testes de desempenho, as equipes podem garantir que seus aplicativos e sistemas sejam capazes de lidar com cargas de trabalho esperadas e fornecer uma experiência do usuário rápida, fluida e responsiva.

3. Prevenção de Falhas em Produção
Falhas de desempenho em produção podem resultar em tempo de inatividade do sistema, perda de dados e danos à reputação da marca. Testes de desempenho rigorosos ajudam a identificar e corrigir potenciais problemas de desempenho antes que eles se tornem críticos e afetem os usuários finais. Isso permite que as equipes evitem custos e impactos negativos associados a falhas em produção.

4. Otimização de Recursos
Testes de desempenho eficazes permitem que as equipes otimizem o uso de recursos, como capacidade de servidor, largura de banda de rede e capacidade de armazenamento. Identificar a capacidade real do sistema e entender como ele se comporta sob diferentes condições de carga ajuda as equipes a tomar decisões informadas sobre alocação de recursos e dimensionamento da infraestrutura.

## Configuração de Pipeline

**Introdução**

Este documento descreve o processo de configuração de uma pipeline de testes de desempenho utilizando as ferramentas K6 e Grafana Cloud. A pipeline permite a execução automatizada de testes de carga em microserviços e a visualização dos resultados em tempo real.

#### GITLAB-CI
````
stages:
  - execute-test-performance

image:
  name: ubuntu:latest

execute-test-performance:
  image:
    name: grafana/k6:latest
    entrypoint: ['']
  stage: execute-test-performance
  script:
    - echo "login with Grafana Cloud "
    - k6 login cloud --token 545b867386d3eb6740ffcb0c487f7e7651a095f79042185eb747a04d83efa1e4
    - echo "executing local k6 in k6 container..."
    - k6 cloud performance/script.js
````


#### TEMPLATE

````
import http from 'k6/http';
import { sleep } from 'k6';

export const options = {
  vus: 5,
  duration: '30s',
  ext: {
    loadimpact: {
      // Project: code project grafana
      projectID: 3684462,
      // name service, will be used as a grouper
      name: 'name-service'
    }
  }
};

export default function() {
  http.get('http://test.k6.io');
  sleep(1);
}
````


**Template**

Arquivo é totalmente editável, é necessário apenas manter o template com as informações do código do grafana onde as métricas serão enviadas após finalizar o processamento dos dados

````
  ext: {
    loadimpact: {
      // Project: poc-performance
      projectID: 3684462,
      // Test runs with the same name groups test runs together.
      name: 'ce-service'
    }
  }
````


**Funcionamento da Pipeline**

_Autenticação com Grafana Cloud:_ O pipeline inicia autenticando-se no Grafana Cloud utilizando um token de acesso.

_Execução de Testes de Desempenho:_ Em seguida, o K6 é executado dentro de um contêiner, utilizando o script fornecido. Este script contém as configurações de teste, como número de usuários virtuais (VUs), duração do teste e URLs a serem testadas.

_Envio de Resultados para Grafana Cloud:_ Durante a execução dos testes, os resultados são enviados para o Grafana Cloud, onde podem ser visualizados e analisados em tempo real.

_Visualização de Métricas:_ No Grafana Cloud, as métricas de desempenho são visualizadas através de painéis e gráficos personalizados, permitindo uma análise detalhada do desempenho do sistema.

- [Referência - K6 Options](https://k6.io/docs/using-k6/k6-options/reference/)
- [How To - K6 Options](https://k6.io/docs/using-k6/k6-options/how-to/)

## Conclusão

A configuração da pipeline de testes de desempenho com K6 e Grafana Cloud oferece uma maneira eficiente de avaliar o desempenho de microserviços e identificar possíveis gargalos. A visualização em tempo real dos resultados facilita a tomada de decisões e a otimização do sistema para melhor desempenho.

![image-1](docs/images/print-1.png)
![image-2](docs/images/print-2.png)
![image-3](docs/images/print-3.png)
![image-4](docs/images/print-4.png)
![image-5](docs/images/print-5.png)
![image-6](docs/images/print-6.png)